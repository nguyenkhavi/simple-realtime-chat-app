import dotenv from "dotenv";
dotenv.config({});
import express from "express";
import http from "http";
import cors from "cors";
import { Server } from "socket.io";
import RealtimeService from "./services/Realtime/index.js";

const app = express();
app.use(cors());
const server = http.createServer(app);

app.io = new Server(server, {
    cors: {
        origin: "*",
    },
});

// app.io.on("connection", (socket) => {
//     console.log(`User Connected: ${socket.id}`);

//     socket.on("join_room", (data) => {
//         socket.join(data);
//         console.log(`User with ID: ${socket.id} joined room: ${data}`);
//     });

//     socket.on("send_message", (data) => {
//         socket.to(data.room).emit("receive_message", data);
//     });

//     socket.on("disconnect", () => {
//         console.log("User Disconnected", socket.id);
//     });
// });
const port = process.env.PORT || 3001;
server.listen(port, () => {
    RealtimeService.createConnection(app.io);
    console.log(` > Server is running on port: ${port}`);
});
