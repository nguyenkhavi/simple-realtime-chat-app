const sockets = {};

const createConnection = (io) => {
    io.on("connection", (socket) => {
        console.log("Socket connection", socket.id);
        sockets[socket.handshake.query.id] = socket.id;

        socket.on("disconnecting", async () => {
            for (const room of socket.rooms) {
                const payload = await io.getSocketsInRoom(room);
                const data = { type: "leave", payload };
                io.to(room).emit("data", { room, data });
            }
            delete sockets[socket.handshake.query.id];
            console.log("Disconnect", socket.id);
        });

        socket.on("subscribe", async ({ room, type }, callback) => {
            socket.join(room);
            const payload = await io.getSocketsInRoom(room);
            const data = { type, payload };
            if (typeof callback === "function") callback(data);
            socket.to(room).emit("data", { room, data });
            console.log("Joined room", room);
        });

        socket.on("unsubscribe", async ({ room, type }, callback) => {
            if (!room) {
                if (typeof callback === "function")
                    callback("Room name is required.");
            } else {
                socket.leave(room);
                const payload = await io.getSocketsInRoom(room);
                const data = { type, payload };
                io.to(room).emit("data", { room, data });
                console.log("Leaving room", room);
            }
        });

        socket.on("send", async ({ room, type, body }, callback) => {
            const data = { type, payload: body };
            if (room) {
                socket.to(room).emit("data", { room, data });
                if (typeof callback === "function") callback(data);
            }
        });
    });

    io.getSocketById = (user_id) => {
        return io.sockets.sockets.get(sockets[user_id]);
    };
    io.getSocketsInRoom = async (roomName) => {
        return await io
            .in(roomName)
            .fetchSockets()
            .then((_sockets) => {
                return _sockets.reduce((p, c) => {
                    p[c.handshake.query.id] = c.id;
                    return p;
                }, {});
            });
    };
    // io.send = (room, data) => {
    //     io.to(room).emit("data", { room, data });
    // };
};

const RealtimeService = {
    createConnection,
};
export default RealtimeService;
