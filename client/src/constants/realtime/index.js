const IO_TYPES = {
    JOIN: "join",
    LEAVE: "leave",
    NEW: "new",
};
export default IO_TYPES;
