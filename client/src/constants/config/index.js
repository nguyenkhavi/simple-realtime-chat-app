const isDev = process.env.NODE_ENV === "development";
export const config = {
    AUTH_URL: isDev
        ? "http://localhost:5000"
        : "https://auth-server.kha-vivi.repl.co",
    REALTIME_URL: isDev
        ? "http://localhost:3001"
        : "https://realtime-server.kha-vivi.repl.co",
    GOOG_API_KEY:
        "126531190563-8088elu0a94jtq7d7h12iq5uurrb0tkv.apps.googleusercontent.com",
};
