import "./App.css";
import React from "react";
import axios from "axios";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Chatroom from "./screens/Chatroom";
import Home from "./screens/Home";

import authAPIs from "./apis/auth";
axios.interceptors.request.use(
    (config) => {
        const token = localStorage.getItem("@access_token_flm");

        if (token) {
            // config.headers["Authorization"] = 'Bearer ' + token;  // for Spring Boot back-end
            config.headers["x-access-token"] = token; // for Node.js Express back-end
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);
axios.interceptors.response.use(
    (res) => {
        return res;
    },
    async (err) => {
        const originalConfig = err.config;

        if (originalConfig.url !== "/auth/login" && err.response) {
            // Access Token was expired
            if (err.response.status === 401 && !originalConfig._retry) {
                originalConfig._retry = true;

                try {
                    const refreshToken =
                        localStorage.getItem("@refresh_token_flm");
                    const { accessToken } = await authAPIs.refreshToken({
                        refreshToken,
                    });
                    localStorage.setItem("@access_token_flm", accessToken);
                    return axios.request(originalConfig);
                } catch (_error) {
                    return Promise.reject(_error);
                }
            }
        }

        return Promise.reject(err);
    }
);
function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/chatroom/:room" element={<Chatroom />} />
            </Routes>
        </BrowserRouter>
    );
}

export default App;
