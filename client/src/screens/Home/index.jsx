import React, { useState, memo } from "react";
import Login from "../../components/Login";
import { useNavigate } from "react-router-dom";
import { useAppContext } from "../../context";
const Home = () => {
    const navigate = useNavigate();
    const [context] = useAppContext();

    const [username, setUsername] = useState(context.user?.username || "");
    const [room, setRoom] = useState("");
    React.useEffect(() => {
        setUsername(() => context.user.username);
        return () => {};
    }, [context.user.username]);
    const joinRoom = () => {
        if (username !== "" && room !== "") {
            navigate(`/chatroom/${room}`, { state: { username } });
        }
    };

    return (
        <div className="App">
            <div className="joinChatContainer">
                <h3>Join A Chat</h3>
                <input
                    value={username}
                    type="text"
                    placeholder="John..."
                    onChange={(event) => {
                        setUsername(event.target.value);
                    }}
                />
                <input
                    type="text"
                    placeholder="Room ID..."
                    onChange={(event) => {
                        setRoom(event.target.value);
                    }}
                />

                <button onClick={joinRoom}>Join A Room</button>
                {!context.accessToken && <Login />}
            </div>
        </div>
    );
};

export default memo(Home, () => true);
