import React, { useEffect, useState, memo } from "react";
import ScrollToBottom from "react-scroll-to-bottom";
import RealtimeService from "../../services/RealtimeService";
import IO_TYPES from "../../constants/realtime";
import { useLocation, useParams } from "react-router-dom";
const Chatroom = () => {
    const { room } = useParams();
    const { state } = useLocation();
    const username = state?.username || "no name";
    const [currentMessage, setCurrentMessage] = useState("");
    const [messageList, setMessageList] = useState([]);
    const [active, setActive] = React.useState(0);
    const sendMessage = async () => {
        if (currentMessage !== "") {
            const messageData = {
                id: `${Math.random() * 2382173837}`,
                room: room,
                author: username,
                message: currentMessage,
                time:
                    new Date(Date.now()).getHours() +
                    ":" +
                    new Date(Date.now()).getMinutes(),
            };

            RealtimeService.send({
                room,
                type: IO_TYPES.NEW,
                body: messageData,
                callback: ({ type, payload }) => {},
            });
            setMessageList((list) => [...list, messageData]);
            setCurrentMessage("");
        }
    };

    useEffect(() => {
        if (!room) return;
        RealtimeService.subscribe({
            room,
            type: IO_TYPES.JOIN,
            fn: ({ type, payload, ...s }) => {
                switch (type) {
                    case IO_TYPES.NEW:
                        setMessageList((list) => [...list, payload]);
                        break;
                    case IO_TYPES.JOIN:
                    case IO_TYPES.LEAVE:
                        setActive(() => Object.keys(payload).length || 0);
                        break;
                    default:
                }
            },
        });
        return () => {
            RealtimeService.unsubscribe({ room, type: IO_TYPES.LEAVE });
        };
    }, [room]);

    return (
        <div className="App">
            <div className="chat-window">
                <div className="chat-header">
                    <p>Live Chat </p>
                    <p>{active} is active</p>
                </div>
                <div className="chat-body">
                    <ScrollToBottom className="message-container">
                        {messageList.map((messageContent) => {
                            return (
                                <div
                                    key={JSON.stringify(messageContent)}
                                    className="message"
                                    id={
                                        username === messageContent.author
                                            ? "you"
                                            : "other"
                                    }
                                >
                                    <div>
                                        <div className="message-content">
                                            <p>{messageContent.message}</p>
                                        </div>
                                        <div className="message-meta">
                                            <p id="time">
                                                {messageContent.time}
                                            </p>
                                            <p id="author">
                                                {messageContent.author}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            );
                        })}
                    </ScrollToBottom>
                </div>
                <div className="chat-footer">
                    <input
                        type="text"
                        value={currentMessage}
                        placeholder="Hey..."
                        onChange={(event) => {
                            setCurrentMessage(event.target.value);
                        }}
                        onKeyPress={(event) => {
                            event.key === "Enter" && sendMessage();
                        }}
                    />
                    <button onClick={sendMessage}>&#9658;</button>
                </div>
            </div>
        </div>
    );
};

export default memo(Chatroom, () => true);
