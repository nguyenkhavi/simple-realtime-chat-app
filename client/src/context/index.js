import React, { useContext, createContext } from "react";
import axios from "axios";
import { config } from "../constants/config";
import RealtimeService from "../services/RealtimeService";
import authAPIs from "../apis/auth";

export const Context = createContext(null);

export const Provider = ({ children, value }) => {
    const [context, setContext] = React.useState(value);
    const connected = React.useRef(false);

    React.useEffect(() => {
        const accessToken =
            window.localStorage.getItem("@access_token_flm") || "";

        if (accessToken) {
            axios.defaults.headers["x-access-token"] = accessToken;
            setContext((s) => ({
                ...s,
                loading: false,
                accessToken,
            }));

            authAPIs.check().then((res) => {
                const { accessToken, user } = res;
                window.localStorage.setItem(
                    "@access_token_flm",
                    res.accessToken
                );
                axios.defaults.headers["x-access-token"] = accessToken;
                setContext((s) => ({
                    ...s,
                    loading: false,
                    user: user,
                }));
            });
        }
    }, []);

    React.useEffect(() => {
        if (context.user && !connected.current) {
            try {
                const id = context.user.id;

                RealtimeService.connect(config.REALTIME_URL, { id }, (res) => {
                    console.log("socket-io connected", res);
                });
            } catch (e) {
                console.log("socket-io error", e);
            }
        }
    }, [context.user]);

    const values = React.useMemo(() => [context, setContext], [context]);

    return <Context.Provider value={values}>{children}</Context.Provider>;
};

export function useAppContext() {
    const context = useContext(Context);

    if (!context) {
        console.error("Error deploying Context!!!");
    }

    return context;
}
