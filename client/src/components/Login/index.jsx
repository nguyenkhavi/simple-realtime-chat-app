import React, { useState } from "react";
import GoogleLogin from "react-google-login";
import { FcGoogle } from "react-icons/fc";
import { config } from "../../constants/config";
import authAPIs from "../../apis/auth";
import { useAppContext } from "../../context";
function Login() {
    const [setContext] = useAppContext();
    const [setState] = useState({
        isLoading: false,
    });
    const handleLogInSuccess = (res) => {
        const { accessToken, refreshToken, user } = res;

        // dispatch(actSetUserData({ user, accessToken }));
        setContext(() => ({ user, accessToken }));
        localStorage.setItem("@access_token_flm", accessToken);
        localStorage.setItem("@refresh_token_flm", refreshToken);
        // history.push("/");
    };

    const handleLogInGoogle = ({ accessToken }) => {
        setState((s) => ({
            ...s,
            isLoading: true,
        }));
        authAPIs
            .logInWithGoogle({ token: accessToken })
            .then((res) => {
                handleLogInSuccess(res);
                setState((s) => ({
                    ...s,
                    isLoading: false,
                }));
            })
            .catch((e) => {
                console.log(e);
                setState((s) => ({
                    ...s,
                    isLoading: false,
                }));
            });
    };
    return (
        <GoogleLogin
            clientId={config.GOOG_API_KEY}
            onSuccess={handleLogInGoogle}
            render={(renderProps) => (
                <div onClick={renderProps.onClick}>
                    <FcGoogle size="40px" style={{ marginRight: 8 }} />
                </div>
            )}
        />
    );
}

export default Login;
