import io from "socket.io-client";
class RealtimeClient {
    handlers = [];
    isConnected = false;

    connect(url, query = {}, callback) {
        this.socket = io(url, { query });
        this.socket.on("connect", () => {
            const id = this.socket.id;
            if (typeof callback === "function") callback(id);
            this.handlers.forEach((handler) => {
                this.socket.emit(
                    "subscribe",
                    {
                        room: handler.room,
                        type: handler.type,
                    },
                    (data) => {
                        handler.function(data);
                    }
                );
            });
        });
        this.socket.on("connect_error", (e) => {
            if (typeof callback === "function") callback(e);
            throw e;
        });

        this.socket.on("data", ({ data, room }) => {
            this.handlers.forEach((handler) => {
                if (handler.room === room) {
                    if (typeof handler.function === "function")
                        handler.function(data);
                }
            });
        });
    }
    subscribe({ room, type, fn }) {
        const handler = { room, type, function: fn };

        if (!this.socket) {
            if (typeof fn === "function") {
                this.handlers.push(handler);
            }
        } else {
            this.socket.emit("subscribe", { room, type }, (data) => {
                if (typeof fn === "function" && data) fn(data);
                this.handlers.push(handler);
            });
        }
    }

    unsubscribe({ room, type, fn }) {
        if (!this.socket) {
            this.handlers = this.handlers.filter((handler) => {
                if (typeof fn === "function" && handler.function !== fn) {
                    return true;
                } else if (typeof fn === "string" && handler.room !== fn) {
                    return true;
                }
                return false;
            });
        } else {
            this.socket.emit("unsubscribe", { room, type }, () => {
                this.handlers = this.handlers.filter((handler) => {
                    if (typeof fn === "function" && handler.function !== fn) {
                        return true;
                    } else if (typeof fn === "string" && handler.room !== fn) {
                        return true;
                    }
                    return false;
                });
            });
        }
    }

    send({ room, type, body, callback }) {
        this.socket.emit("send", { room, type, body }, callback);
    }
}

const RealtimeService = new RealtimeClient();

export default RealtimeService;
